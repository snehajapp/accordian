import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.css']
})
export class AccordionComponent implements OnInit {

  @Input('data') data = {};
  @Input('bgColor') bgColor = 'rgb(182, 136, 224)';
  @Input('color') color = 'black';


  openIt = false;
  closeIt = false;

  buttonOpen = true;
  buttonClose = false;


  constructor() {

  }

  ngOnInit() {

  }

  open() {
    console.log('open');
    this.openIt = true;
    this.closeIt = false;

    this.buttonOpen = false;
    this.buttonClose = true;
  }

  close() {
    console.log('close');
    this.closeIt = true;
    this.openIt = false;

    this.buttonOpen = true;
    this.buttonClose = false;
  }

}
